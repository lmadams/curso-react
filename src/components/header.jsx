import * as React from "react";
import './header.css';

class Header extends React.Component{
  render() {
    return (
      <div className={'header-container'}>
        {this.props.titulo}
      </div>
    );
  }
}

export default Header;
