import * as React from "react";

class Button extends React.Component{

  onClickButton() {
    console.log('CLICK:');
  }

  render() {
    return (
      <button onClick={this.onClickButton}>
        {this.props.nome}
      </button>
    );
  }
}

export default Button;
