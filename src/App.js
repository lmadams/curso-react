import React from 'react';
import Titulo from "./components/titulo";
import Header from "./components/header";
import Button from "./components/button";

function App() {
  return (
    <>
      <Header titulo={'Aplicação'}/>
      <div>
        <Titulo title={'Ola mundo'}/>

        <Button nome={'Botão 1'}/>
        <Button nome={'Botão 2'}/>
        <Button nome={'Botão 3'}/>
      </div>
    </>
  );
}

export default App;
